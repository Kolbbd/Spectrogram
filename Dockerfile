FROM python:3.10
RUN apt-get update && apt -y upgrade && apt -y install ffmpeg libsndfile-dev
RUN pip3 install pydub librosa matplotlib
WORKDIR /ds
COPY main.py ./
COPY test.wav ./
CMD python3 main.py
