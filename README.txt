Brett Kolb
Dr. Dymacek
11/29/22

Spect-'o'u-gram
-A audio splitting program that makes 255x255 Spectrograms of bird calls.



To Build:
	docker build -t spectro .

To set up Docker:
	docker run --rm -dit --name spectro spectro

To execute the code:
	docker exec -it spectro /bin/bash

	python3 main.py

To retrieve images:
	docker exec -it spectro /usr/bin/scp output#.png kolbbd@192.168.50.100:ds/dataout/

To stop Docker:
	docker stop spectro
	docker rm spectro





