#Sources:https://python.tutorialink.com/how-can-i-split-an-audio-file-into-multiple-audio-wav-files-from-folder/, https://www.tutorialspoint.com/how-to-save-a-librosa-spectrogram-plot-as-a-specific-sized-image 

#Brett David Kolb
#11/29/2022
#Dr.Dymacek


import math
import numpy as np
import matplotlib.pyplot as plt
import librosa
import librosa.display
import os

from pydub import AudioSegment
from pydub.silence import split_on_silence
from pydub.utils import make_chunks
from scipy import signal
from scipy.io import wavfile


plt.rcParams["figure.figsize"] = [2.55,2.55]
plt.rcParams["figure.autolayout"] = True

fig, ax = plt.subplots()
hl = 512#Number of samples in Spectrogram
hi = 256#Height of image
wi = 256#Width of image

print("Please type the file you would like to process.")
file = input()

print("Please input sample length in milliseconds.")
chunk_length_ms = input()

sound_file = AudioSegment.from_wav(file)
chunks = make_chunks(sound_file, int(chunk_length_ms))

#sets up output directory
parent_dir = os.getcwd()
output = "output"
path = os.path.join(parent_dir, output)
os.mkdir(path)

num_of_chunks = 0
for i, chunk in enumerate(chunks):
  chunk_name = "{0}.wav".format(i)
  print("Spliting", chunk_name)
  chunk.export(chunk_name, format="wav")
  num_of_chunks += 1

while(num_of_chunks > 0):
  y, sr = librosa.load(str(num_of_chunks-1)+".wav")#librosa.ex(str(num_of_chunks-1)+".wav"))
  window = y[0:wi*hl]

  S = librosa.feature.melspectrogram(y=window, sr=sr, n_mels=hi, fmax=8000, hop_length=hl)
  S_db = librosa.power_to_db(S, ref=np.max)
  img = librosa.display.specshow(S_db, x_axis="off", y_axis="off", sr=sr, fmax=8000, ax=ax)

  plt.savefig("output/"+"output"+str(num_of_chunks-1)+".png")
  plt.show()
  num_of_chunks-=1


